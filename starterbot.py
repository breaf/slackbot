# -*- coding: utf-8 -*-
import time
import requests
import json
from slackclient import SlackClient


# starterbot's ID as an environment variable
# BOT_ID = os.environ.get("BOT_ID")
BOT_ID = 'U75PG00JJ'

# constants
AT_BOT = "<@" + BOT_ID + ">"
EXAMPLE_COMMAND = "do"
RATES_COMMAND = "get rates"
finance_ua_api_url = 'http://resources.finance.ua/ru/public/currency-cash.json'

api_token = 'xoxb-243798000630-SyUyON8SoCaz9hNSonlx7ijs'
# slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))
slack_client = SlackClient(api_token)


def handle_command(command_text, channel_number):
    """
        Receives commands directed at the bot and determines if they
        are valid commands. If so, then acts on the commands. If not,
        returns back what it needs for clarification.
    """
    print 'command: ', command_text
    print 'channel: ', channel_number
    response = "Not sure what you mean. Use the *" + EXAMPLE_COMMAND + "* command with numbers, delimited by spaces."
    if command_text.startswith(EXAMPLE_COMMAND):
        response = "Sure...write some more code then I can do that!"
    if command_text.startswith(RATES_COMMAND):
        response = get_rates()
    slack_client.api_call("chat.postMessage", channel=channel_number,
                          text=response, as_user=True)


def parse_slack_output(slack_rtm_output):
    """
        The Slack Real Time Messaging API is an events firehose.
        this parsing function returns None unless a message is
        directed at the Bot, based on its ID.
    """
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                # return text after the @ mention, whitespace removed
                return output['text'].split(AT_BOT)[1].strip().lower(), output['channel']
    return None, None


def get_rates():
    bank_id = '7oiylpmiow8iy1smaqn'  # Банк восток
    # make request
    response_text = requests.get(finance_ua_api_url).text
    response = json.loads(response_text)
    organizations_list = response['organizations']
    # build organizations dict
    organizations = {organization['id']: organization for organization in organizations_list}
    # get currencies
    bank = organizations[bank_id]
    currencies = bank['currencies']
    usd = currencies['USD']
    eur = currencies['EUR']
    # prepare result
    result = u"Банк \"Восток\" USD: {0} / {1}, EUR: {2} / {3}".format(usd['bid'], usd['ask'], eur['bid'], eur['ask'])
    return result


if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 2  # 1 second delay between reading from firehose
    if slack_client.rtm_connect():
        print("StarterBot connected and running!")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            if command and channel:
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")
